
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './component/header'
import Footer from './component/footer'
import Home from './pages/home'
import About from './pages/about'
import Detail from './pages/detail'
import Pokemon from './pages/pokemon'
import Favorit from './pages/favorit'

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="body">
      <BrowserRouter>
      <Header/>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/detail" element={<Detail />} />
          <Route path="/pokemon" element={<Pokemon />} />
          <Route path="/favorit" element={<Favorit />} />
        </Routes>
      <Footer/>
      </BrowserRouter>
    </div>
  );
}

export default App;
