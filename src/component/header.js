import { Link } from "react-router-dom";

function App() {
    return (
        <div className='header'>
            <div className='brand'>
                <Link to="/">
                    <img src={require('../images/brand.png')} />
                </Link>
            </div>
            <div className='navlink'>
                <Link to="/pokemon">
                    <a className="btn_list">
                        List Pokemon
                    </a>
                </Link>
                <Link to="/favorit">
                    <a className="btn_favorit">
                        Favorit
                    </a>
                </Link>
                <Link to="/about">
                    <a className="btn_about">
                        About Team
                    </a>
                </Link>
            </div>
        </div>
    );
}

export default App;
