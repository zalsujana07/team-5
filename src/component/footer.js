import { Link } from "react-router-dom";

function App() {
    return (
        <div className='footer'>
            <div className='credit1'>
                <div className='brand'>
                    <Link to="/">
                        <img src={require('../images/brand.png')} />
                    </Link>
                    <p>
                    Pokémon adalah salah satu waralaba media yang dimiliki oleh perusahaan permainan video Nintendo dan diciptakan oleh Satoshi Tajiri pada 1995. Pada mulanya, Pokémon adalah seri permainan video yang identik dengan konsol Game Boy.
                    </p>
                </div>
            </div>
            <div className='credit2'>
                <div className="title">The Pokedex Company</div>
                <ul>
                    <li>
                        What's New
                    </li>
                    <li>
                        Pokémon Parents Guide
                    </li>
                    <li>
                        About Our Company
                    </li>
                    <li>
                        Pokémon Careers
                    </li>
                    <li>
                        Select a Country/Region
                    </li>
                    <li>
                        Pokémon Press Site
                    </li>
                </ul>
            </div>
            <div className='credit2'>

            </div>

        </div>
    );
}

export default App;
