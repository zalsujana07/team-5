import React, { useEffect, useState } from "react";
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import './pokemon.css';

function App() {
  const [post, setPost] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    fetchData()
  }, []);

  async function fetchData() {
    setIsLoading(true);
    axios.get("https://pokedexbackend.herokuapp.com/pokemon")
      .then((response) => {
        setPost(response.data.result);
        console.log(response.data);
        setIsLoading(false)
      })
      .catch(() => {
        setIsLoading(false);
      });
  }

  return (
    <div className="content_pokemon">
      <div className="pencarian">
        pencarian
      </div>
      {isLoading ?
        <div className="spinner-container">
          <div className="loading-spinner">
          </div>
        </div>
        :
        <Row className="list-item">
          {
            post && post.map((el, index) => (
              <Col md={2} sm={4} className="item">
                <div className="content">
                  <a className="desc-1">#{index}</a>
                  <img src={el.pictureBack} />
                  <a className="desc-2">{el.name}</a>
                  <div className="desc-3">
                    <a>Favorit</a>
                  </div>
                </div>
              </Col>
            ))
          }
        </Row>}
    </div>
  );
}

export default App;
