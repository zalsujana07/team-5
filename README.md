Membuat Website Pokemon Dengan Requirement:
1. Home Page
   Memiliki minimal 3 buah content/banner.
2. Pokemon Page
   Menampilkan list pokemon dari fetching API.
3. About Team
   Menampilkan informasi tim berupa:
	nama, 
	photo, 
	background, dan 
	experience (optional)
4. Detail Pokemon
   Menampilkan detail pokemon dari fetching API.




Requirement Tambahan Wajib:
1. Menampilkan Navbar dan Footer di setiap page.
2. Menggunakan state management Redux.
3. Menggunakan konsep navigation.


Requirement Tambahan Point Plus:
1. Menggunakan Middleware Redux (Redux Saga)
2. Customize View Pokemon (Pokemon dan Detail Page)
3. Pagination (Pokemon dan Detail Page)
4. Fitur Search Pokemon (Pokemon dan Detail Page)
5. Favorite Menu (Add / Delete Favorite) (Tidak bisa menambahkan pokemon yang sama)
6. Caching Data (Redux Persist)
7. Page untuk Not Found (http://localhost:3000/blablabla)



Teknis:
Setiap kelompok wajib menggunakan repositori pribadi.
Urutan Presentasi akan diumumkan pada hari Jumat.

API:
GET https://pokedexbackend.herokuapp.com/pokemon (All Pokemon)
GET https://pokedexbackend.herokuapp.com/pokemon/:id (Detail Pokemon)

Example: 
https://pokemon-ke.netlify.app/pokemon